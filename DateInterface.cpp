//DateInterface
//CS 3307 Group 16
//Date Interface CPP file
//November 8th 2018: Stage Three

#include <DateInterface.h>

//Public constructor of Date Interface
DateInterface::DateInterface(){
	t = time(NULL);
}

//Gets and returns the value of getDate() as a string value
std::string DateInterface::getDate(){
	tStruct = localtime(&t);
    std::strftime(date, sizeof(date), "%A %B %d, %Y", tStruct);
	tString = "The date is "+std::string(date);
	return tString;
}

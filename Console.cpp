//Console
//CS 3307 Group 16
//Console CPP file
//November 8th 2018: Stage Three

#include <Console.h>

//Public constructor for console
Console::Console()
{
	d = NULL;
	state = 0;
}

//Public create_window() method will generate and fill the GUI to present on screen to user
void Console::create_window()
{
	if (d != NULL)
		return;

	d = XOpenDisplay(NULL);
	state = DefaultScreen(d);
	w = XCreateSimpleWindow(d, RootWindow(d, state), 10, 10, 500, 500, 1, BlackPixel(d, state), WhitePixel(d, state));
	XSelectInput(d, w, ExposureMask | KeyPressMask);
	XMapWindow(d, w);
	XFlush(d);
}

//Public delete_window() destroys the window generated prior
void Console::delete_window()
{
	if (d != NULL)
		XCloseDisplay(d);
	d = NULL;
}

//Public setDate() recieves a the date information and fills the open console with informatin
void Console::setDate(const std::string &tdate){
	if (d == NULL)
		return;
	XDrawString(d, w, DefaultGC(d, state), 10, 50, tdate.c_str(), tdate.size());
	XFlush(d);
}

//Public setTime() recieves a the date information and fills the open console with informatin
void Console::setTime(const std::string &time){
	if (d == NULL)
		return;
	XDrawString(d, w, DefaultGC(d, state), 10, 100, time.c_str(), time.size());
	XFlush(d);
}

//Public deconstructor
Console::~Console()
{
	delete_window();
}

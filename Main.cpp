//Main
//CS 3307 Group 16
//Main CPP file
//November 8th 2018: Stage Three

#include <iostream>
#include <string.h>
#include <Console.h>
#include <TimeInterface.h>
#include <DateInterface.h>
#include <FakeSensor.h>

int main(){
	//As 0-10 is generated, a theshold of five will give equal chance to display or not display console
	const uint_fast64_t THRESHOLD = 5;
	
	//Create objects associated with their respective classes by calling constructors
	Console display;
	Sensor *sensor = new FakeSensor();
	TimeInterface timeIface;
	DateInterface dateIface;
	
	//Will continiously call and display the console pased upon return value of the FakeSensor object. The display will be destroyed should the threshold be lost.
	while (true) {
		sensor->set_val();
	
		if (sensor->get_val() < THRESHOLD) {
			display.create_window();
			display.setTime(timeIface.getTime());
			display.setDate(dateIface.getDate());
		} else {
			display.delete_window();
		}

		sleep(1);
	}

	//Free sensor object to free seed
	delete sensor;
	
	return 0;
}

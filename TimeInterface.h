//Time Interface
//CS 3307 Group 16
//Time Interface Header file
//November 8th 2018: Stage Three

#ifndef MEMORY_H
#define MEMORY_H

#include <ctime>
#include <string>

//Time Interface class definition
class TimeInterface{
	private:
		//Protected variables for needed for time interface and allows for inheritence
		time_t currentTime;
		struct tm *timeStruct;
		std::string outStr;
		char timeChars[30];
	
	public:
		//Publically accessable constructor and getTime() value
		TimeInterface();
		std::string getTime();
		
};

#endif

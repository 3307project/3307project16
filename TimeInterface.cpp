//TimeInterface
//CS 3307 Group 16
//Time Interface CPP file
//November 8th 2018: Stage Three

#include <ctime>
#include <string.h>
#include <TimeInterface.h>

//Public constructor to start time menu
TimeInterface::TimeInterface(){
	//Constructor does little since getTime() has to constantly update the time
	outStr = "The current time is ";
}

//Updates live time menu with current value of time, returning as a string
std::string TimeInterface::getTime(){
	currentTime = time(NULL);
	timeStruct = localtime(&currentTime);
	std::strftime(timeChars, sizeof(timeChars), "%I:%M", timeStruct);
	return outStr + std::string(timeChars);
}

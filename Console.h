//Console Interface
//CS 3307 Group 16
//Console Interface Header file
//November 8th 2018: Stage Three

#ifndef CONSOLE_H
#define CONSOLE_H

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string>

const std::string msg = "Welcome to our 3307 group project application!";

//Console class definition
class Console {
protected:
	//Protected variables to open console, allows for inheritence
	Display *d;
	Window w;
	XEvent e;
	int state;
public:
	//Constructor, destructor and methods used by the console
	Console();
	void create_window();
	void delete_window();
	void setDate(const std::string&);
	void setTime(const std::string&);
	~Console();
};

#endif
//FakeSensor Interface
//CS 3307 Group 16
//FakeSensor Interface Header file
//November 8th 2018: Stage Three

#ifndef FAKESENSOR_H
#define FAKESENSOR_H

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <Sensor.h>

//Date Interface class definition extends from sensor class
class FakeSensor : public Sensor {
//Publically accessable constructor and setVal() and getVal() value
public:
	FakeSensor();
	virtual void set_val();
	virtual uint_fast64_t get_val();
};

#endif

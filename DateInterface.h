//Date Interface
//CS 3307 Group 16
//Date Interface Header file
//November 8th 2018: Stage Three

#ifndef Date_H
#define Date_H

#include <ctime>
#include <string>

//Date Interface class definition
class DateInterface{
	//Protected variables for needed for date interface and allows for inheritence
	private:
		time_t t;
		struct tm * tStruct;
		std::string tString;
		char date[30];
	//Publically accessable constructor and getDate() value
	public:
		DateInterface();
		std::string getDate();
};

#endif
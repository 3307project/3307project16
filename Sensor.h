//Sensor Interface
//CS 3307 Group 16
//Sensor Interface Header file
//November 8th 2018: Stage Three

#ifndef SENSOR_H
#define SENSOR_H

#include <stdint.h>

//Sensor Interface class definition
class Sensor {
//Protected variables for needed for date interface and allows for inheritence to FakeSensor()
protected:
	uint_fast64_t sensor_val;
public:
//Publically accessable constructor and setVal() and getVal() value
	virtual void set_val() = 0;
	virtual uint_fast64_t get_val() = 0;
};

#endif